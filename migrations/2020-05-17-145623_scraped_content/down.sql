PRAGMA legacy_alter_table=ON;

ALTER TABLE articles RENAME TO _articles_old;

CREATE TABLE articles (
	article_id TEXT PRIMARY KEY NOT NULL,
	title TEXT,
	author TEXT,
	feed_id TEXT NOT NULL REFERENCES feeds(feed_id),
	url TEXT,
	timestamp DATETIME NOT NULL,
	html TEXT,
	summary TEXT,
	direction INTEGER,
	unread INTEGER NOT NULL,
	marked INTEGER NOT NULL
);

INSERT INTO articles (article_id, title, author, feed_id, url, timestamp, html, summary, direction, unread, marked)
  SELECT article_id, title, author, feed_id, url, timestamp, html, summary, direction, unread, marked
  FROM _articles_old;

DROP TABLE _articles_old;

PRAGMA legacy_alter_table=OFF;