use crate::models::{Category, Enclosure, FatArticle, Feed, FeedMapping, Headline, Tag, Tagging};

pub struct SyncResult {
    pub feeds: Option<Vec<Feed>>,
    pub categories: Option<Vec<Category>>,
    pub mappings: Option<Vec<FeedMapping>>,
    pub tags: Option<Vec<Tag>>,
    pub headlines: Option<Vec<Headline>>,
    pub articles: Option<Vec<FatArticle>>,
    pub enclosures: Option<Vec<Enclosure>>,
    pub taggings: Option<Vec<Tagging>>,
}

impl SyncResult {
    pub fn decompose(
        self,
    ) -> (
        Option<Vec<Feed>>,
        Option<Vec<Category>>,
        Option<Vec<FeedMapping>>,
        Option<Vec<Tag>>,
        Option<Vec<Headline>>,
        Option<Vec<FatArticle>>,
        Option<Vec<Enclosure>>,
        Option<Vec<Tagging>>,
    ) {
        (
            self.feeds,
            self.categories,
            self.mappings,
            self.tags,
            self.headlines,
            self.articles,
            self.enclosures,
            self.taggings,
        )
    }
}
